import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFormatterPopoverComponent } from './text-formatter-popover.component';

describe('TextFormatterPopoverComponent', () => {
  let component: TextFormatterPopoverComponent;
  let fixture: ComponentFixture<TextFormatterPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFormatterPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFormatterPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
