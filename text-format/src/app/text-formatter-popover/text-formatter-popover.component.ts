import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-text-formatter-popover',
  templateUrl: './text-formatter-popover.component.html',
  styleUrls: ['./text-formatter-popover.component.less']
})
export class TextFormatterPopoverComponent implements OnInit {

  @Input() isBold = false;
  @Input() isItalic = false;
  @Input() isUnderline = false;
  @Input() offsetTop = 0;
  @Input() offsetLeft = 0;

  @Output() changeBold = new EventEmitter<boolean>();
  @Output() changeItalic = new EventEmitter<boolean>();
  @Output() changeUnderline = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {}

}
