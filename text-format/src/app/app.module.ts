import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { TextFormatterDirective } from './directives/text-formatter.directive';
import { TextFormatterPopoverComponent } from './text-formatter-popover/text-formatter-popover.component';

@NgModule({
  declarations: [
    AppComponent,
    TextFormatterDirective,
    TextFormatterPopoverComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule
  ],
  entryComponents: [TextFormatterPopoverComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
