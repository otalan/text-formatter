import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  ElementRef,
  HostListener,
  ViewContainerRef
} from '@angular/core';
import { TextFormatterPopoverComponent } from '../text-formatter-popover/text-formatter-popover.component';
import { Subscription } from 'rxjs/internal/Subscription';

@Directive({
  selector: '[appTextFormatter]'
})

export class TextFormatterDirective {

  popoverComponentRef: ComponentRef<TextFormatterPopoverComponent>;
  subscriptions: Subscription[] = [];

  @HostListener('mouseup', ['$event'])
  @HostListener('keyup', ['$event'])
  handleSelection(event: any) {
    if (this.popoverComponentRef) {
      this.unlistenEmitters();
      this.popoverComponentRef.destroy();
    }
    const selectionRange = window.getSelection().getRangeAt(0);
    if (selectionRange.startContainer !== selectionRange.endContainer) {
      selectionRange.setEnd(selectionRange.startContainer, selectionRange.startContainer.textContent.length)
    }

    if (selectionRange.toString()) {
      this.createComponent(event);
      this.listenEmitters(selectionRange);
    }
  }

  constructor(
    private element: ElementRef,
    private viewContainerRef: ViewContainerRef,
    private componentFactory: ComponentFactoryResolver
  ) { }

  checkPreFormat(selectionHtml: string, formatType: string): boolean {
    return selectionHtml.includes(`<${formatType}>`)
      || selectionHtml.includes(`</${formatType}>`);
  }

  listenEmitters(selection: Range): void {
    this.subscriptions.push(
      this.popoverComponentRef.instance.changeBold.subscribe(value => {
        this.popoverComponentRef.instance.isBold = value;
        if (value) {
          selection.surroundContents(document.createElement('strong'));
        } else {
          const textNode = document.createTextNode(selection.cloneContents().textContent);
          selection.deleteContents();
          selection.insertNode(textNode);
        }
      }),
      this.popoverComponentRef.instance.changeItalic.subscribe(value => {
        this.popoverComponentRef.instance.isItalic = value;
        if (value) {
          selection.surroundContents(document.createElement('i'));
        } else {
          const textNode = document.createTextNode(selection.cloneContents().textContent);
          selection.deleteContents();
          selection.insertNode(textNode);
        }
      }),
      this.popoverComponentRef.instance.changeUnderline.subscribe(value => {
        this.popoverComponentRef.instance.isUnderline = value;
        if (value) {
          selection.surroundContents(document.createElement('u'));
        } else {
          const textNode = document.createTextNode(selection.cloneContents().textContent);
          selection.deleteContents();
          selection.insertNode(textNode);
        }
      })
    )
  }

  unlistenEmitters(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createComponent(event: any) {
    const popoverFactory = this.componentFactory.resolveComponentFactory(TextFormatterPopoverComponent);
    const selectionHtml = this.getSelectionHtml();
    this.popoverComponentRef = this.viewContainerRef.createComponent(popoverFactory);
    this.popoverComponentRef.instance.offsetTop = event.clientY;
    this.popoverComponentRef.instance.offsetLeft = event.clientX;
    this.popoverComponentRef.instance.isBold = this.checkPreFormat(selectionHtml, 'strong');
    this.popoverComponentRef.instance.isItalic = this.checkPreFormat(selectionHtml, 'i');
    this.popoverComponentRef.instance.isUnderline = this.checkPreFormat(selectionHtml, 'u');
  }

  getSelectionHtml() {
    // gently reused from https://stackoverflow.com/questions/5222814/window-getselection-return-html
    let html = '';
    if (typeof window.getSelection !== 'undefined') {
      const sel = window.getSelection();
      if (sel.rangeCount) {
        const container = document.createElement('div');
        for (let i = 0, len = sel.rangeCount; i < len; ++i) {
          container.appendChild(sel.getRangeAt(i).cloneContents());
        }
        html = container.innerHTML;
      }
    } else if (typeof (document as any).selection != 'undefined') {
      if ((document as any).selection.type == 'Text') {
        html = (document as any).selection.createRange().htmlText;
      }
    }
    return html;
  }

}
