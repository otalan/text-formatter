# text-formatter

In order to build project locally, follow these steps:

- pull the project
- run *npm install* in the 'text-format' directory to install packages
- run *ng serve* to build Angular application
- navigate to *localhost:4200* with your browser

Synonyms are not implemented. 
And also there is an unhandled case when user selects a text that contains separately formatted parts. If so, application will automove end of selection to the end of the first holistic part.

e.x. selecting 
> **Bold text** *Italic text*

will auto reselect to 

> **Bold text** 


p.s. there's also no modularity, everything is in app-component and a single directive, i did no pay attention on that.